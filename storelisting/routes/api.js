const route = require("express").Router();

const Store = require("../models/store");
const User = require("../models/user");

//get Stores - GET
route.get("/stores",(req,res,next)=>{
    Store.getStores(15,09,(stores)=>{
        console.log(stores);
    });   
});

//Like a store - PUT
route.put("/stores/:name",(req,res,next)=>{
    User.likeStore(req.body.user._id,req.params.name,(err,user)=>{
        if(err) throw err;
        res.status(200);
        res.send("added successfully");
    });
});

//Dislike a store - DELETE
route.delete("/stores/:name",(req,res,next)=>{
    User.dislikeStore(req.body.user._id,req.params.name,(err,user)=>{
        if(err) throw err;
        res.status(200);
        res.send("deleted successfully");
    });
});

module.exports = route ;