const route = require("express").Router();
const mongoose = require("mongoose");
const passport = require("passport");
const localStrategy = require("passport-local").Strategy;
const User = require("../models/user");

//cookie start Session
passport.serializeUser((user,done)=>{
    done(null, user.id);
});

//cookie end Session
passport.deserializeUser((id,done)=>{
    User.getUserById(id,(err,user)=>{
        done(err,user);
    });
});

//local Strategy 
passport.use(new localStrategy((username,password,done)=>{
    User.getUserByUsername(username,(err,user)=>{
        console.log("getUserbyEmail",user);
        if(err) throw err;
        if(!user){
        console.log("Ce compte n'existe pas")
        return done(null, false, {error_msg: 'Ce compte n\'existe pas'});
        }
        console.log("password",password,"user.pass",user.password);
        if(password==user.password){
            console.log(user)
            return done(null, user);
        } else{
            console.log("Mot de passe incorecte")
            return done(null, false , {message :'Mot de passe incorecte'});
        }
    });
}));

function ensureAuthenticated(req,res,next){
    if(req.isAuthenticated()){
      return next();
      req.flash('error_msg', 'Vous n\'êtes pas autorisé à voir ce contenu!');
    } else{
      res.send('/login');
    }
}

// Add user - POST
route.post("/register",(req,res,next)=>{
    if(!req.body.username || !req.body.password){
        res.json({success: false, message : "username or password not found!"});
      }else {
        var utc = new Date().toJSON().slice(0,10).replace(/-/g,'/');
        var newUser = new User({
            username : req.body.username,
            name : req.body.name,
            password : req.body.password,
            lastLogin : utc,
            likedStores:[]
        });
  
        //saving the user
        newUser.save((err, userSaved)=>{
          console.log("ERROR", userSaved);
          if(err){
            return err
          }
          res.redirect("/cpanel/Authenticate",{
              user:{
                username: req.body.username,
                last_login: req.body.last_login
              }
          });
        });
      }
    });  

// Login - POST
route.post("/login",(req,res,next)=>{
    console.log("email",req.body.email,"pass", req.body.password);
    const errors=[]
    if(req.body.username=="")  errors.push("username is required!");
    if(req.body.name=="")   errors.push("name is required!");
    if(req.body.password=="")   errors.push("password is required!");
    console.log("errors",errors);
    if(errors.length!==0){
        res.send({
            errors:errors
        });
    }else{
        passport.authenticate('local',{
            successRedirect:'/loged_in',
            failureRedirect:'/failure',
            failureFlash: false
          })(req,res,next);        
    }

/*    passport.authenticate('local',{
        successRedirect:'/',
        failureRedirect:'/cpanel/signup',
        failureFlash: false
      })(req,res,next);
  */  
});

route.get("/loged_in",ensureAuthenticated ,(req,res,next)=>{
    res.send("connected!");
});
route.get("/failure",(req,res,next)=>{
    res.send("connected!");
});


module.exports = route;