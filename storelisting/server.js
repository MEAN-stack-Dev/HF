const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const bcryptjs = require("bcryptjs");
const path = require("path");
const passport = require("passport");
const cookieParser = require("cookie-parser");
const expressSession = require("express-session");

const app = express();

const main = require("./config/main")
const api = require("./routes/api");
const usr = require("./routes/usr");

//pulic folder
app.use(express.static(path.join(__dirname,"public")));


// bodyParser midlleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended:false
}));

//connecting to the DB
mongoose.connect(main.dataBaseUrl,()=>{
    console.log("connected successfully",main.dataBaseUrl);
});
const db = mongoose.connection;

//--------------- login system midllewares

//express Session midlleware
app.use(expressSession({
    secret: "secret",
    saveUninitialized:true,
    resave:true
}));

// passport midlleware
app.use(passport.initialize());
app.use(passport.session());



//--------------- login system midllewares

app.use("/api",api);
app.use("/usr",usr);

app.listen(main.port,()=>{
    console.log("server running","port",main.port);
})