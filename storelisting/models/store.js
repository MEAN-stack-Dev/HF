const mongoose = require("mongoose");
var sortJsonArray = require('sort-json-array');

const storeSchema = mongoose.Schema({
    name:{
        type:String,
        required:true,
        unique:true
    },
    longitude:{
        type:Number,
        required:true
    },
    latitude:{
        type:Number,
        required:true,
    }
});

const Store = module.exports =  mongoose.model("Store",storeSchema);

module.exports.getStores = (userLng,userLat,callback)=>{
    var Stores = Store.find((err,Stores)=>{
        for(var i=0; i <Stores.length; i++){
            Stores[i]["longitude"]= Math.abs(Stores[i]["longitude"]-userLng);
            Stores[i]["latitude"]= Math.abs(Stores[i]["latitude"]-userLat);
            Stores[i]["distance"]= Math.sqrt(Math.pow(Stores[i]["longitude"],2)+ Math.pow(Stores[i]["latitude"],2));
            console.log("Store dist",Stores[i]["name"],Stores[i]["distance"])
        }
        return callback(null,Stores); 
    });
    //console.log(Stores);
    
}

