const mongoose = require("mongoose");
const bcryptjs = require("bcryptjs");

const userSchema = mongoose.Schema({
    name:{
        type:String,
        unique:true,
        required:true
    },
    username:{
        type:String,
        unique:true,
        required:true
    },
    password:{
        type:String,
        required:true
    },
    lastLogin : {
        type : String
    },
    likedStores:[{
        type:String,
        unique:true
    }]
});

const User = module.exports = mongoose.model("User",userSchema);

module.exports.saveUser = (newUser,callback)=>{
    bcryptjs.genSalt(10,(err,salt)=>{
        if(err) throw err;
        bcryptjs.hash(newUser.password,salt,(err,hash)=>{
            if(err) throw err;
            newUser.password=hash;
            console.log("newUser.password",newUser.password);
            newUser.save(callback);
        });
    });
}

module.exports.getUserById = (id,callback)=>{
    const query = {_id:id};
    return User.findById(query,callback);
}

module.exports.getUserByUsername = (username,callback)=>{
    const query = {username:username};
    return User.findOne(query,callback);
}

module.exports.comparePassword = (userPassword,hash,callback)=>{
    bcryptjs.compare(userPassword,hash,(err,isMatch)=>{
        if(err) throw err;
        callback(null,isMatch);
    });
}

module.exports.likeStore = (id,store,callback)=>{
    User.findById(id,(err,user)=>{
        if(err) throw err;
        user.likedStores.push(store);
        user.save(callback);
    });
}


module.exports.dislikeStore = (id,store,callback)=>{
    User.findById(id,(err,user)=>{
        if(err) throw err;
        else{
            for(var i = 0 ; i <user.likedStores.length;i++){
                console.log(store,user.likedStores);
                if(user.likedStores[i] == store) {
                    user.likedStores.splice(i,1);
                    i--;
                }
            }
            console.log("user.likedStores",user.likedStores);
            user.save(callback);
        }
    })
}